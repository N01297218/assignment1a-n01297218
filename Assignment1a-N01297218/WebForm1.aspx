﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Assignment1a_N01297218.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Hotel Reservation</h1>


            <label>Name:</label><br />
            <asp:TextBox runat="server" ID="customerName" placeholder="Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Name" ControlToValidate="customerName" ID="validatorName"></asp:RequiredFieldValidator>
            <br />

            <label>Cell Number:</label><br />
            <asp:TextBox runat="server" ID="customerPhone" placeholder="Phone"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Phone Number" ControlToValidate="customerPhone" ID="validatorPhone"></asp:RequiredFieldValidator>
            <br />
            
            <label>E-mail:</label><br />
            <asp:TextBox runat="server" ID="customerEmail" placeholder="Email"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an Email" ControlToValidate="customerEmail" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="customerEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            <br />

            <label>Number of Guests:</label><br />
            <asp:TextBox runat="server" ID="customerNumber" placeholder="Number of Guests"></asp:TextBox>
            <asp:RangeValidator runat="server" ControlToValidate="customerNumber" Type="Integer" MinimumValue="1" MaximumValue="4" ErrorMessage="No More than 4 guests"></asp:RangeValidator>
            <br />

            <label>Types of Room:</label><br />
            <asp:DropDownList runat="server" ID="roomtype">
                <asp:ListItem Value="SR" Text="Superior Room"></asp:ListItem>
                <asp:ListItem Value="LR" Text="Luxury Room"></asp:ListItem>
                <asp:ListItem Value="LS" Text="Luxury Suite"></asp:ListItem>
                <asp:ListItem Value="SK" Text="Superior One king"></asp:ListItem>
                <asp:ListItem Value="RV" Text="Room with view"></asp:ListItem>
            </asp:DropDownList>
            <br />

            <label>Facilities Preferred:</label>
            <div id="utilities" runat="server">
            <asp:CheckBox runat="server" ID="utilities1" Text="WiFi" />
            <asp:CheckBox runat="server" ID="utilities2" Text="Room Service" />
            <asp:CheckBox runat="server" ID="utilities3" Text="Spa/Massage" />
            <asp:CheckBox runat="server" ID="utilities4" Text="Pool Parties" />
            </div>
            <br />

            <label>Payment Options:</label>
            <asp:RadioButton runat="server" Text="Cash" GroupName="via"/>
            <asp:RadioButton runat="server" Text="Debit/Visa" GroupName="via"/>
            <br />

            <asp:Button runat="server" ID="myButton"  Text="Confirm Booking"/>
            <br />
            <div runat="server" ID="res"></div>

        </div>
    </form>
</body>
</html>
