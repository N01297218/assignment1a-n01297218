﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1a_N01297218
{
    public class Client
    {
        private string cusName;
        private string cusPhone;
        private string cusEmail;
       

        public Client()
        {

        }
        public string CusName
        {
            get { return cusName; }
            set { cusName = value; }
        }
        public string CusPhone
        {
            get { return cusPhone; }
            set { cusPhone = value; }
        }
        public string CusEmail
        {
            get { return cusEmail; }
            set { cusEmail = value; }
        }
    }

}